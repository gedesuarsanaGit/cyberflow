/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cyberflow.model;

import com.cyberflow.util.CalculationUtil;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author gede
 */
public class CyberflowLineComponent extends CyberflowComponent{
    
  private CyberflowComponent sourceComponent;
  
   private CyberflowComponent destinationComponent;
   
   

    public CyberflowComponent getSourceComponent() {
        return sourceComponent;
    }

    public void setSourceComponent(CyberflowComponent sourceComponent) {
        this.sourceComponent = sourceComponent;
    }

    public CyberflowComponent getDestinationComponent() {
        return destinationComponent;
    }

    public void setDestinationComponent(CyberflowComponent destinationComponent) {
        this.destinationComponent = destinationComponent;
    }
    
  public  List<Point> points = new ArrayList<Point>();    
    
  
 
  public boolean isLineCollideWithMouse(MouseEvent e){
  boolean output= false;
  
  Point mousePoint = e.getPoint();
  
  int currentX = (int) mousePoint.getX();
  
  int currentY = (int) mousePoint.getY();
  
  int expectedX = (int) mousePoint.getX();
  
  int expectedY;
      expectedY = CalculationUtil.getYPointbyXPoint((int)mousePoint.getX(), this.getSourceComponent().getPosition(), this.getDestinationComponent().getPosition());
  
  //box collition detection
      
 if( ((expectedX-15) < currentX) && ((expectedX+15) > currentX)   &&  ((expectedY-15) < currentY) && ((expectedY+15) > currentY) )  {
 
 output = true;
 
 }
  
      
  
  return output;
  }
  
  
  
  public Point getArrowLocation(){
  Point output = new Point();

  int y =0;
  int x =0;
  
  Point source = this.getSourceComponent().getPosition();
 // Point destination = this
  
//  if(){}

  
  

  return output;
  }
  
  
}
