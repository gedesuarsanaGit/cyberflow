/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cyberflow.service;

import com.cyberflow.model.Cyberflow;
import com.cyberflow.model.CyberflowComponent;
import com.cyberflow.model.CyberflowLineComponent;

/**
 *
 * @author gede
 */
public class CyberflowService {

    private static Cyberflow cyber;

    public static Cyberflow getCurrentCyberflow() {
        if (cyber == null) {
            cyber = new Cyberflow();
        }
        return cyber;
    }

    
    public static void unSelectAllCyberflowComponents(){
    
    for(CyberflowComponent item : CyberflowService.getCurrentCyberflow().componentList)
    {
    item.setSelected(false);
    }
   
    for(CyberflowLineComponent item :CyberflowService.getCurrentCyberflow().lineList )
    {
    item.setSelected(false);
    }
    
    
    }
    
    
    
}
