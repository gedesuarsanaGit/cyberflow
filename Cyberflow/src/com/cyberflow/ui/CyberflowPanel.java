/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cyberflow.ui;

import com.cyberflow.model.Cyberflow;
import com.cyberflow.model.CyberflowComponent;
import com.cyberflow.model.CyberflowLineComponent;
import com.cyberflow.service.CyberflowService;
import com.cyberflow.util.CalculationUtil;
import com.cyberflow.util.ConstantUtil;
import com.cyberflow.util.ImageUtil;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Stroke;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.geom.AffineTransform;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *
 * @author gede
 */
public class CyberflowPanel extends JPanel implements Runnable, MouseListener, MouseMotionListener {

    public CyberflowPanel() {

        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        Thread panelThread = new Thread(this);
        panelThread.setName("JPanel Cyberflow Thread");
        panelThread.start();
    }

    private int x = 0;
    private int y = 0;

    private static int cursorX;
    private static int cursorY;

    private boolean isDragged = false;

    public static boolean isAttachingMode = false;

    public static CyberflowComponent selectedComponent = null;

    @Override
    protected void paintComponent(Graphics g) {

        super.paintComponent(g);

        long startTime = System.currentTimeMillis();

        // start paint component
        // drawing moved rectangle
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.BLUE);
        g2.drawRect(x, y, 100, 100);

        // drawing cyberflowcomponent
        drawCyberflow(g2, CyberflowService.getCurrentCyberflow());

        // System.out.println(CyberflowService.getCurrentCyberflow().componentList.size());
// end paint component
        long endTime = System.currentTimeMillis();
        long delay = endTime - startTime;

        // System.out.println("delay:" + delay);
        if (delay >= 0) {
            long expectedDelay = 1000 / 60; //60fps
            //   System.out.println("expectedDelay:" + expectedDelay);
            long remainingDelay = expectedDelay - delay;
            if (remainingDelay >= 0) {
                try {
                    //         System.out.println("remainingDelay:" + remainingDelay);
                    Thread.sleep(remainingDelay);
                } catch (InterruptedException ex) {
                    Logger.getLogger(CyberflowPanel.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        y++;
        x++;

    }

    @Override
    public void run() {

        while (true) {

            this.repaint();

        }

    }

    public void drawCyberflow(Graphics2D g2d, Cyberflow cyber) {

         
        
        // line component
        for(CyberflowLineComponent item: cyber.lineList){
        
        CyberflowComponent source =  item.getSourceComponent();
       
        CyberflowComponent destination =   item.getDestinationComponent();
        
        int sourceX = (int)source.getPosition().getX()+35;
        int sourceY = (int) source.getPosition().getY()+35;
        int destinationX =(int)destination.getPosition().getX()+35;
        int destinationY = (int)destination.getPosition().getY()+35;
        
        if(item.isSelected()){
            g2d.setColor(Color.gray);
          //  g2d.setStroke(new BasicStroke(2));
            Stroke dashed = new BasicStroke(2, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
           g2d.setStroke(dashed);
            g2d.drawLine(sourceX,sourceY, destinationX,destinationY);
        }else{
            g2d.setColor(Color.black);
            g2d.setStroke(new BasicStroke(2));
            g2d.drawLine(sourceX,sourceY, destinationX,destinationY);
        }
        
        
        
        
        
        double degree = CalculationUtil.getAngleFromPoint(source.getPosition(), destination.getPosition());
         
       // System.out.println("derajat:"+degree);
        
        
    
       
        
        Point middlePoint = CalculationUtil.getMiddlePoint(new Point(sourceX,sourceY),new Point(destinationX,destinationY));
        
        
           AffineTransform at = new AffineTransform();
            
       
          at.translate(middlePoint.getX()-15, middlePoint.getY()-15);
          
          
        //  g2d.drawOval((int)middlePoint.getX(), (int)middlePoint.getY(), 10, 10);
          
       //   at.rotate(Math.toRadians(-90));
        //  at.rotate(Math.toRadians(degree));
               at.rotate(Math.toRadians(-90), 15, 15);
               at.rotate(Math.toRadians(degree), 15, 15);
        
           g2d.drawImage(ImageUtil.imageMap.get("arrow"), at, this);
        
                
                }
        
        
         g2d.setColor(Color.black);
         Stroke dashed = new BasicStroke(1, BasicStroke.CAP_BUTT, BasicStroke.JOIN_BEVEL, 0, new float[]{9}, 0);
         g2d.setStroke(dashed);
        
// basic component
        for (CyberflowComponent item : cyber.componentList) {

            g2d.drawImage(item.getImage(), (int) item.getPosition().getX(), (int) item.getPosition().getY(), null);

            if (item.isSelected()) {
                if (!isAttachingMode) {
                    g2d.setColor(Color.black);
                    g2d.drawRect((int) item.getPosition().getX(), (int) item.getPosition().getY(), (int) item.getWidht(), (int) item.getHeight());
                } else {
                    g2d.setColor(Color.black);
                    g2d.drawRect((int) item.getPosition().getX(), (int) item.getPosition().getY(), (int) item.getWidht(), (int) item.getHeight());
                      
                            int sourceX = (int)item.getPosition().getX()+35;
                            int sourceY = (int) item.getPosition().getY()+35;
    
                    g2d.drawLine(sourceX, sourceY, cursorX, cursorY);
                }

            }
            
            g2d.setColor(Color.black);
            g2d.drawString(item.getName(), (int) item.getPosition().getX()-35, (int) item.getPosition().getY()+(int)item.getHeight()+10);
            

        }
        
        

    }

    @Override
    public void mouseClicked(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

        CyberflowComponent compSelected = checkMouseCollideWithComponents(e);
        
        CyberflowService.unSelectAllCyberflowComponents();
        
       //for select unselect
        if (compSelected != null) {
            if (compSelected.isSelected() == false) {
                compSelected.setSelected(true);
            } else {
                compSelected.setSelected(false);
            }
            
      //  System.out.println("isSelected:"+compSelected.isSelected() );
            
        }
        
        // for select attaching line
        if (isAttachingMode) {
            if (compSelected == null) {
                isAttachingMode = false;
            } else {
                //attaching to destination create Object Line;
            
                  // create line
                CyberflowLineComponent newLine = new CyberflowLineComponent();
                
                newLine.setName(ConstantUtil.line_name+ConstantUtil.getLine_number());
                newLine.setSourceComponent(selectedComponent);
                newLine.setDestinationComponent(compSelected);
                newLine.setType("line");
                newLine.points.add(selectedComponent.getPosition());
                newLine.points.add(compSelected.getPosition());
               
                
                CyberflowService.getCurrentCyberflow().lineList.add(newLine);
                isAttachingMode =false;
                
            }
        }

        selectedComponent = compSelected;
        
        // create component on the canvas 
        if (compSelected == null) {
            if (CyberflowFrame.selectedComponentId == ConstantUtil.excel_file_source_id) {

                CyberflowComponent comp = new CyberflowComponent();
                comp.setImage(ImageUtil.imageMap.get("excel_file_source"));
                Point createPoint = new Point();
                createPoint.x = e.getX() - 35;
                createPoint.y = e.getY() - 35;

                comp.setPosition(createPoint);
                comp.setName(ConstantUtil.excel_file_source_name+ConstantUtil.getExcel_file_source_number());
                comp.setType("excel_file_source");
                comp.setWidht(70);
                comp.setHeight(70);

                CyberflowService.getCurrentCyberflow().componentList.add(comp);

                CyberflowFrame.selectedComponentId = ConstantUtil.default_cursor_id;
                setCursor(Cursor.getDefaultCursor());

            }
        }

        // context Menu right click
        if (compSelected != null) {
            if (e.isPopupTrigger()) {
                CyberflowFrame.componentPopupMenu.show(this, e.getX(), e.getY());
            }
        }

    }

    @Override
    public void mouseReleased(MouseEvent e) {

        if (isDragged) {
            Point dragPoint = new Point();
            dragPoint.x = e.getX() - 35;
            dragPoint.y = e.getY() - 35;

            selectedComponent.setPosition(dragPoint);
            CyberflowService.getCurrentCyberflow().componentList.add(selectedComponent);
            setCursor(Cursor.getDefaultCursor());
            isDragged = false;
            selectedComponent = null;
        }

    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public CyberflowComponent checkMouseCollideWithComponents(MouseEvent e) {
        CyberflowComponent output = null;

        for (CyberflowComponent item : CyberflowService.getCurrentCyberflow().componentList) {

            if (item.isCollideWithMouse(e)) {
                output = item;

            }

        }
        
        if(output == null){
        for(CyberflowLineComponent item : CyberflowService.getCurrentCyberflow().lineList){
        
            if(item.isLineCollideWithMouse(e)){
            output = item;
            }
        
        }
        }
        

        return output;
    }

    @Override
    public void mouseDragged(MouseEvent e) {

        if (isDragged == false) {
            CyberflowComponent draggedItem = checkMouseCollideWithComponents(e);

            if (draggedItem != null) {
                
                if(!draggedItem.getType().equalsIgnoreCase("line")){
                CyberflowService.getCurrentCyberflow().componentList.remove(draggedItem);

                CyberflowFrame.jPanel3.setCursor(ImageUtil.cursorMap.get("excel_file_source_cursor"));
                selectedComponent = draggedItem;
                isDragged = true;
                }
            }
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

        CyberflowPanel.cursorY = e.getY();
        CyberflowPanel.cursorX = e.getX();

    }

}
