/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cyberflow.util;

import java.awt.Point;

/**
 *
 * @author gede
 */
public class CalculationUtil {

    
    
    
    public static int getYPointbyXPoint(int x, Point source, Point destination) {
        int y = 0;

        double sourceX = source.getX() + 35;
        double sourceY = source.getY() + 35;
        double destinationX = destination.getX() + 35;
        double destinationY = destination.getY() + 35;

        if ((source != null) && (destination != null)) {
            double slope = ((destinationY - sourceY) / (destinationX - sourceX));
            double b = (sourceY - (slope * sourceX));
            y = (int) ((slope * x) + b);

        }

        return y;
    }

    public static double getAngleFromPoint(Point firstPoint, Point secondPoint) {

        if ((secondPoint.x > firstPoint.x)) {//above 0 to 180 degrees

            return (Math.atan2((secondPoint.x - firstPoint.x), (firstPoint.y - secondPoint.y)) * 180 / Math.PI);

        } else if ((secondPoint.x < firstPoint.x)) {//above 180 degrees to 360/0

            return 360 - (Math.atan2((firstPoint.x - secondPoint.x), (firstPoint.y - secondPoint.y)) * 180 / Math.PI);

        }//End if((secondPoint.x > firstPoint.x) && (secondPoint.y <= firstPoint.y))

        return Math.atan2(0, 0);

    }
    
    
    
    public static Point getMiddlePoint(Point source,Point destination){
    Point output = new Point();
    output.setLocation( ((destination.getX() -  source.getX())/2) + source.getX(), ((destination.getY()  - source.getY())/2)+ source.getY() );
    return output; 
    }
    
    

}
