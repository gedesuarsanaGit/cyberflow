/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cyberflow.util;

/**
 *
 * @author gede
 */
public class ConstantUtil {
    
    
    public static String line_name = "line";
    
    public static String excel_file_source_name = "Excel_File_Source";
    
    
    public static int line_id = 99;
    
    public static int default_cursor_id = 0;
    
    public static int excel_file_source_id = 1;
    
    
    
    private static int line_number = 0;
    
    private static int excel_file_source_number = 0;

    public static int getLine_number() {
        return line_number++;
    }

    public static int getExcel_file_source_number() {
        return excel_file_source_number++;
    }
    
    
    
    
    
    
}
