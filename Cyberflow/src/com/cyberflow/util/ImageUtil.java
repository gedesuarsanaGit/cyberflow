/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cyberflow.util;

import java.awt.Cursor;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;

/**
 *
 * @author gede
 */
public class ImageUtil {
    
     public static Map<String,Image> imageMap=  new HashMap<String,Image>();
     public static Map<String,Cursor> cursorMap = new HashMap<String,Cursor>(); 
     public static Toolkit toolkit = Toolkit.getDefaultToolkit();
     
     
     public static void loadAllImage(){
            Image  image1 = new ImageIcon(ImageUtil.class.getResource("/resource/excel_icon_kecil.png")).getImage();
            imageMap.put("excel_file_source", image1);
            
               Image  image2 = new ImageIcon(ImageUtil.class.getResource("/resource/arrow_kecil.png")).getImage();
               imageMap.put("arrow", image2);
            
     }
     
     public static void loadAllCursor(){
     
       Cursor excel_source_cursor = ImageUtil.toolkit.createCustomCursor(ImageUtil.imageMap.get("excel_file_source"),new Point(35,35), "excel_file_source");
       cursorMap.put("excel_file_source_cursor", excel_source_cursor);
      
     }
     
     
   
}
